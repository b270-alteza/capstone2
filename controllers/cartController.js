const Cart = require("../models/Cart");
const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");
const productController = require("./productController");
const userController = require("./userController");

module.exports.addToCart = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	let betterUserData = await User.findById(userData.id)

	let userId = userData.id;
	let cartId = betterUserData.cartId;

	//const cartData = await getCart(cartId);
	console.log(`UserId is ${userId}`)
	console.log(`CartId is ${cartId}`)

	if(!userData.isAdmin) {
		let totalAmount = 0;
		let products = [];
		let productIds = [];
		let productQuantitys = [];
		let addToCartProduct = req.body.addToCart; 

		//console.log(`Getting order count`)

		let thisPrice;
		let { productName, size, quantity } = addToCartProduct[0];
		console.log(addToCartProduct)
		let thisProduct = await productController.getProductByName(productName)
		//let thisProduct;
		//thisProduct = await Product.findOne({ productName: productName });

		//console.log(thisProduct)
		if (quantity == 0) {
			res.send(false)
		}

		console.log(`Quantity: ${quantity}`)

		if (size == undefined) {
			size = "None"
		}
		//console.log(`size is: ${size}`)

		while (thisProduct == undefined) {
			res.send(`data not yet feteched`)
		}
		//console.log(`Exits getProductByName function`)

		if (size = "None") {
			thisPrice = thisProduct.price[0];
			console.log(`Entered else in price ${thisPrice}, ${thisProduct.price[0]}`)
		} else {
			let sizeIdx = thisProduct.size.indexOf(size);
			thisPrice = thisProduct.productPrice[sizeIdx];
		}

		// console.log(`thisPrice ${thisPrice}, thisProduct.productPrice[0] ${thisProduct.productPrice[0]}, size ${size}`)
		// //console.log(`Exits getProductByName function`)

		let qtyInCart;
		let cart = await Cart.findOne({ _id: cartId })
		if (!cart) {
			console.log('Cart not found');
		}
		const product = await cart.products.find(
		  (product) => product.productName === productName
		);
		console.log(`product: ${product}`);
		if (product) {
			qtyInCart = product.productQuantity;
		}	

		let totalCartQuantity = parseInt(quantity) + parseInt(qtyInCart)
		if (totalCartQuantity > thisProduct.stock) {
			console.log(
`Quantity of ${productName} to be ordered is invalid!
Remaining quantity is ${thisProduct.stock}.`
)
			res.send(false)
		}


		totalAmount = thisPrice * totalCartQuantity;

		if (qtyInCart > 0) {
			const cart = await Cart.findOneAndUpdate(
				{ _id: cartId, 'products.productName': thisProduct.productName },
				{ $set: { 'products.$.productQuantity': totalCartQuantity,'products.$.subTotal': totalAmount } },
				{ new: true }
			)

			if (cart) {
				console.log("Product quantity updated!")
			} else {
				res.send(false);
			}
		} else if (quantity > 0) {
			totalAmount = thisPrice * quantity;		
			products.push({
				productId: thisProduct.id,
				productName: thisProduct.productName,
				productSize: size,
				productPrice: thisPrice,
				productQuantity: quantity,
				subTotal: totalAmount
			})
			await Cart.findOneAndUpdate({ _id: cartId },  { $push: { products: products } }, {new:true})
			//await Cart.findOneAndUpdate({ _id: cartId }, { $push: { products: products } })
			.then(result => {
			    //console.log(result)
			    console.log("Cart updated!")
			})
			.catch(error => {
			    console.log(error);
			    console.log("Cart failed to update!")
			    res.send(false)
			})
		} else {
			res.send(false)
		}

		const isTotalUpdated = await updateCartTotalAmount(cartId);
		if (isTotalUpdated) {
			console.log(`Total successfully updated!`)
		} else {
			console.log(`Total failed to update!`)
		}
		res.send(true);

	} else {
		console.log("The user is an Admin.")
		return res.send(false);
	}
	
}

module.exports.editProductCount = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let betterUserData = await User.findById(userData.id)

	let userId = userData.id;
	let cartId = betterUserData.cartId;
	if (!userData.isAdmin) {
		let thisProduct = await productController.getProductByName(req.body.productName);
		let newSubtotal = thisProduct.price * req.body.quantity;

		Cart.findOneAndUpdate(
			{ _id: cartId, 'products.productName': req.body.productName },
			{ $set: { 'products.$.productQuantity': req.body.quantity,'products.$.subTotal': newSubtotal } },
			{ new: true }
		).then(result => {
			console.log("This is the updated card product")
			console.log(result)
			updateCartTotalAmount(cartId)
			console.log("Product quantity updated!")
			res.send(true);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		console.log(`Admin not authorized.`)
		res.send(false)
	}
}

module.exports.makeCart = () => {

	let newCart = new Cart({
		userId: "temp"
	});

	return newCart.save()
	.then(cart => {
		console.log(cart);
		return cart.id
	})
	.catch(error => {
		console.log(error);
		return false
	})
}

module.exports.changeUserId = async (cartId, userId, userEmail) => {
    let changeUserData = {
        userId: userId,
        email: userEmail
    }
    return await Cart.findByIdAndUpdate(cartId, changeUserData).then(result => {
        console.log(result)
        return true
    })
    .catch(error => {
        console.log(error);
        return false
    })
}

async function updateCartTotalAmount(cartId) {
	await Cart.findOneAndUpdate(
		{ _id: cartId },
		[ { $set: { totalAmount: { $sum: '$products.subTotal'} } } ],
		{ new: true }
	)
	.then( (updatedCart) => {
		//console.log('Updated cart:', updatedCart);
		return true
	})
	.catch((error) => {
		console.error('Error updating cart:', error);
		return false
	});
}

module.exports.deleteProduct = async(req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let betterUserData = await User.findById(userData.id)

	let userId = userData.id;
	let cartId = betterUserData.cartId;
	if (!userData.isAdmin) {
		Cart.findOneAndUpdate(
			{ _id: cartId },
			{ $pull: { products: { productName: req.body.productName } } },
			{ new: true }
		)
		.then((updatedCart) => {
			updateCartTotalAmount(cartId)
			res.send(true)
		})
		.catch((error) => {
			console.error('Error deleting product from the cart:', error);
			res.sent(false)
		});
	}
}

module.exports.clearCartReq = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let cartId = userData.cartId;

	const clearSuccess = await clearCart(cartId);
	if (clearSuccess) {
		console.log(`Cart clear Success!`)
		res.send(true)
	} else {
		console.log(`Cart clear Failed!`)
		res.send(false)
	}
}

module.exports.clearCartCall = async (cartId) => {
	 let success = await clearCart(cartId);
	 if (success) {
	 	return true
	 } else {
	 	return false
	 }
}

async function clearCart(cartId) {
	await Cart.updateOne(
		{ _id: cartId },
		{ $set: { products: [], totalAmount: 0 } }
	)
	.then(() => {
		console.log('All products deleted from the cart successfully');
		return true
	})
	.catch((error) => {
		console.error('Error deleting products from the cart:', error);
		return false
	});
}

module.exports.getAllInCart = async (cartId, orders) => {
	await Cart.findOne({ _id: cartId })
	.then((cart) => {
		if (!cart) {
			console.log('Cart not found');
			return false;
		}
		orders = cart.products;
		console.log('Orders:', orders);
		return true
	})
	.catch((error) => {
		console.error('Error retrieving cart:', error);
		return false
	});
}

module.exports.getAllInCartReq = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let betterUserData = await User.findById(userData.id)

	let userId = userData.id;
	let cartId = betterUserData.cartId;

	console.log("cartId", cartId)

	await Cart.findOne({ _id: cartId })
	.then((cart) => {
		if (!cart) {
			console.log('Cart not found');
			res.send(false);
		}
		orders = cart.products;
		console.log('Orders:', orders);
		res.send(cart)
	})
	.catch((error) => {
		console.error('Error retrieving cart:', error);
		res.send(false)
	});
}

async function getInCartQuantity(cartId, productName) {

	console.log("cartId", cartId)

	await Cart.findOne({ _id: cartId })
	.then( async (cart) => {
		if (!cart) {
			console.log('Cart not found');
			return 0
		}
		const product = await cart.products.find(
		  (product) => product.productName === productName
		);
		//console.log(`cart: ${cart}`);
		console.log(`product: ${product}`);
		if (product) {
			const qty = product.productQuantity;
			console.log(`productQuantity: ${qty}`);
			return qty;
		}	

		return 0
	})
	.catch((error) => {
		console.error('Error retrieving cart:', error);
		return 0
	});
}