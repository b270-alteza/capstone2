const Order = require("../models/Order");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const User = require("../models/User");
const auth = require("../auth");
const productController = require("./productController");
const userController = require("./userController");
const cartController = require("./cartController");

module.exports.buyProducts = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let betterUserData = await User.findById(userData.id)
	let userId = userData.id;
	let cartId = betterUserData.cartId;
	let totalAmount;

	console.log(cartId)

	await Cart.findOne({ _id: cartId })
	.then((cart) => {
		if (!cart) {
			console.log('Cart not found');
		}
		console.log("cart", cart)
		orders = cart.products;
		totalAmount = cart.totalAmount;
		console.log("totalAmount", totalAmount)
		console.log('Orders:', orders);
	})
	.catch((error) => {
		console.error('Error retrieving cart:', error);
	});

	//console.log(userData);

	//await cartController.getAllInCart(cartId, orders);
	console.log("these are the orders");
	console.log(orders);
	console.log("entering loop");


	console.log(`UserId is ${userId}`)

	if(!userData.isAdmin) {
		let products = [];
		let productIds = [];
		let productQuantitys = [];

		console.log(`getting order count`)
		let newOrCount;
		let orderCount = await Order.countDocuments();
		if (orderCount == 0) {
			newOrCount = 1;
		} else {
			newOrCount = await getMaxOrderCount();
		}

		console.log(`Current order count ${newOrCount}`);

		for (let order of orders) {
			let thisPrice;
			let productName = order.productName
			let size = order.productSize
			let quantity = order.productQuantity
			//let productPrice = productPrice

			//console.log("order: ", order)
			//let { productName, size, quantity } = order;
			let thisProduct = await productController.getProductByName(productName)
			console.log(`This product ${thisProduct}`)
			//let thisProduct;
			//thisProduct = await Product.findOne({ productName: productName });

			//console.log(thisProduct)

			if (size == undefined) {
				size = "None"
			}
			//console.log(`size is: ${size}`)

			while (thisProduct == undefined) {
				res.send(`data not yet feteched`)
			}
			//console.log(`Exits getProductByName function`)

			if (size = "None") {
				thisPrice = thisProduct.price[0];
				console.log(`Entered else in price ${thisPrice}, ${thisProduct.price[0]}`)
			} else {
				let sizeIdx = thisProduct.size.indexOf(size);
				thisPrice = thisProduct.productPrice[sizeIdx];
			}

			// console.log(`thisPrice ${thisPrice}, thisProduct.productPrice[0] ${thisProduct.productPrice[0]}, size ${size}`)
			// //console.log(`Exits getProductByName function`)
			if (quantity > thisProduct.stock) {
				console.log(`Quantity of ${productName} to be ordered is invalid!
					Remaining quantity is ${thisProduct.stock}.`)
				res.send(false)
			}

			products.push({
				productId: thisProduct.id,
				productName: thisProduct.productName,
				productSize: size,
				productPrice: thisPrice,
				productQuantity: quantity
			})

			//totalAmount += thisPrice * quantity;

			console.log(`Iteration result`)
			console.log(products)

			productIds.push(thisProduct.id);
			productQuantitys.push(thisProduct.stock - quantity)
		}

		console.log(`Exited for loop`)

		let newOrder = new Order({
			orCount: newOrCount,
			userId: userData.id,
			totalAmount: totalAmount,
			products: products
		});

		console.log(newOrder)
		console.log(`Done let newOrder = new Order`)

		//res.send(true)

		await newOrder.save()
		.then(order => {
			console.log("Order has been saved.")
			console.log(order);
			//res.send(true)
		})
		.catch(error => {
			console.log(error);
			//res.send(false);
		})	

		
		console.log("Enter getOrderID()")
		let orderID = await getOrderID(newOrCount);
		console.log(`Exit getOrderID(): ${orderID}`)
		//res.send(true)
		let isOrderPushToUserSuccess = await userController.pushOrderIdToUser(userData.id, orderID);


		console.log("Exit pushOrderIdToUser()")
		//res.send(isOrderPushToUserSuccess)

		let isOrderPushToProductSuccess = true;
		let iCount = 0;
		for (let productId of productIds) {
			isOrderPushToProductSuccess = isOrderPushToProductSuccess && await productController.pushOrderIdToProduct(productId, orderID) && await productController.updateProductQuantity(productId, productQuantitys[iCount]);
			iCount++
		}

		let isCartCleared = await cartController.clearCartCall(cartId);

		console.log("isOrderPushToUserSuccess: ", isOrderPushToUserSuccess)
		console.log("isOrderPushToProductSuccess: ", isOrderPushToProductSuccess)

		if (isOrderPushToUserSuccess && isOrderPushToProductSuccess) {
		 	res.send(true)
		} else {
			res.send(false)
		}

	} else {
		console.log("The user is an Admin.")
		return res.send(false);
	}
	
	//res.send(true)
}


async function getMaxOrderCount() {
	let newOrCount;
	const highestOrder = await Order.findOne({})
      .sort({ orCount: -1 }) 
      .limit(1);
	return await Order.findById(highestOrder.id).then(result => {
		console.log(result)
		if (result == null) {
			newOrCount = 0;
		} else {
			console.log(`The current orCount is: ${result.orCount}`)
			newOrCount = result.orCount + 1;
		}
		console.log(newOrCount);
		return newOrCount
	})
	.catch(error => {
		console.log(error);
		return -1
	})
}

async function getOrderID(oldOrCount) {
	return await Order.findOne({ orCount: oldOrCount })
	.then(result => {
		console.log(result);
		return result._id
	})
	.catch(error => {
		console.log(error);
	})
}

module.exports.getOrder = async (req, res) => {
	return await Order.findById(req.params.orderId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}

async function localGetOrder(orderId) {
	return await Order.findById(orderId)
	.then(result => {
		//console.log("get order: ", result)
		return result
	})
	.catch(error => {
		console.log(error);
		return false
	})
}

module.exports.getAllMyOrders = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let betterUserData = await User.findById(userData.id)

	let userId = userData.id;
	let cartId = betterUserData.cartId;

	let orderIds = [];
	let orders = [];

	await User.findOne({ _id: userId })
	.then((user) => {
		if (!user) {
			console.log('Cart not found');
		}
		console.log("user", user)
		orderIds = user.orders;
	})
	.catch((error) => {
		console.error('Error retrieving cart:', error);
	});

	for (let orderId of orderIds) {
		console.log("Getting an order: ", orderId.orderId)
		let order = await localGetOrder(orderId.orderId)
		console.log("get order: ", order)
		orders.push(order);
		console.log("orders array: ", orders)

	}
	res.send(orders)
}

exports.getTopThreeProducts = async (req, res) => {
  try {
    const topThreeProducts = await Order.aggregate([
      {
        $unwind: "$products"
      },
      {
        $group: {
          _id: "$products.productId",
          totalQuantity: { $sum: "$products.productQuantity" }
        }
      },
      {
        $sort: { totalQuantity: -1 }
      },
      {
        $limit: 3
      },
      {
        $project: {
          _id: 0,
          productId: "$_id",
          totalQuantity: 1
        }
      }
    ]);

    res.json(topThreeProducts);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

// async function getAllOrderIds(userId) {
// 	await User.findOne({ _id: userId })
// 	.then((user) => {
// 		if (!user) {
// 			console.log('Cart not found');
// 		}
// 		console.log("user", user)
// 		orders = user.orders;
// 		console.log('Orders:', orders);
// 		return orders
// 	})
// 	.catch((error) => {
// 		console.error('Error retrieving cart:', error);
// 	});
// }

module.exports.getAllOrders = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {Order.find({})
		let allOrders = await Order.find({});
		res.send(allOrders)
	} else {
		res.send(false)
	}
}