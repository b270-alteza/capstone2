const Cart = require("../models/Cart");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const cartController = require("./cartController");

module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		if(result.length > 0) {
			return res.send (true);

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return res.send (false);
		}
	})
	.catch(error => res.send (error))
}

module.exports.changeUserPassword = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	const oldPassword = req.body.oldPassword;
	const newPassword = req.body.newPassword;

	try {
		const user = await User.findById(userData.id);

		const isPasswordCorrect = bcrypt.compareSync(oldPassword, user.password);
		console.log(`Password is: ${isPasswordCorrect}`)
		if (!isPasswordCorrect) {
			res.send(false)
		} else {
			// Hash the new password
			const hashedPassword = await bcrypt.hash(newPassword, 10);

			// Update the user's password
			user.password = hashedPassword;
			await user.save();

			res.send(true)
		}
	} catch (error) {
		console.log(error);
		res.send(false);
	}
};

module.exports.registerUser = async (req, res) => {

	const cartId = await cartController.makeCart();
	if (cartId == false) {
		console.log("Failed to create object cart for user")
		res.send(false)
	}

	let newUser = new User({
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		cartId: cartId
	})

	try {
		const user = await newUser.save();
		console.log(user);
		const updateCart = await cartController.changeUserId(cartId, user.id, req.body.email);
		if (!updateCart) {
			console.log("Failed to update cart after user creation!")
			res.send(false)
		} else {
			console.log("Update cart after user creation success!")
			res.send(true);
		}
	} catch (error) {
		console.log(error);
		res.send(false);
	}
}

module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {

		if(result == null) {

			return res.send({message: "No user found"});

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {

				return res.send({accessToken: auth.createAccessToken(result)});

			} else {
				return res.send(false)
			}
		}
	})
};

module.exports.getProfile = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {

		result.password = "";

		return res.send(result);
	});

};

module.exports.changeUserAdminFlag = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin) {
    	const userEmail = req.body.email;

        let changeUserAdminFlag = {
            isAdmin: req.body.isAdmin
        }

        return User.findOneAndUpdate({ email: userEmail }, changeUserAdminFlag)
        .then(result => {
            console.log(result)
            return res.send(true)
        })
        .catch(error => {
            console.log(error);
            res.send(false)
        })
    } else {
        return res.send(false)
    }
}

module.exports.pushOrderIdToUser = (userId, orderId) => {
    return User.updateOne({ _id: userId }, { $push: { orders: { orderId: orderId } } }).then(result => {
        console.log(result)
        console.log("User orderId updated")
        return true
    })
    .catch(error => {
        console.log(error);
        return false
    })
}