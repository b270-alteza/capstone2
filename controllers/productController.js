const Product = require("../models/Product");
const auth = require("../auth");

module.exports.addProduct = (req, res) => {
	//res.send("Entered addproduct function");
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	//res.send("Authorized");
	if(userData.isAdmin) {
		let newProduct = new Product({
			productName: req.body.productName,
			description: req.body.description,
			price: req.body.price,
			stock: req.body.stock
		});

		return newProduct.save()
		.then(product => {
			console.log(product);
			res.send(true);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	 }
	
}

module.exports.getAllProducts = (req, res) => {

	return Product.find({}).then(result => res.send(result));
}

module.exports.getAllActive = (req, res) => {

	return Product.find({isActive: true}).then(result => res.send(result));
}

module.exports.getProduct = async (req, res) => {

	console.log(req.params.productId)

	return await Product.findById(req.params.productId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}

module.exports.getProductByName = async (productName) => {
  console.log(`Entered getProductByName function: ${productName}`);
  try {
    const result = await Product.findOne({ productName: productName });
    //console.log(result);
    return result;
  } catch (error) {
    console.log(error);
  }
};

module.exports.updateProduct = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		let updateProduct = {
			productName: req.body.productName,
			description: req.body.description,
			price: req.body.price,
			stock: req.body.stock
		}

		return await Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		console.log(`Product registration not authorized!`)
		return res.send(false);
	}
}

module.exports.archiveProduct = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin) {
        let archiveProduct = {
            isActive: req.body.isActive
        }
        return await Product.findByIdAndUpdate(req.params.productId, archiveProduct).then(result => {
        	console.log(`Archiving success!`)
            console.log(result)
            return res.send(true)
        })
        .catch(error => {
            console.log(error);
            res.send(false)
        })
    } else {
        return res.send(false)
    }
}

module.exports.pushOrderIdToProduct = async (productId, orderId) => {
    return await Product.updateOne({ _id: productId }, { $push: { orders: { orderId: orderId } } }).then(result => {
        console.log(result)
        console.log("Product orderId updated!")
        return true
    })
    .catch(error => {
        console.log(error);
        return false
    })
}

module.exports.updateProductQuantity = async (productId, quantity) => {

	return await Product.findByIdAndUpdate(productId,  { stock: quantity }, {new:true})
	.then(result => {
		console.log(result);
		console.log("Product quantity updated!")
		return true;
	})
	.catch(error => {
		console.log(error);
		return false
	})
}

// exports.getTopThreeProducts = async (req, res) => {
//   try {
//     const topThreeProducts = await Order.aggregate([
//       {
//         $unwind: "$products"
//       },
//       {
//         $group: {
//           _id: "$products.productId",
//           totalQuantity: { $sum: "$products.productQuantity" }
//         }
//       },
//       {
//         $sort: { totalQuantity: -1 }
//       },
//       {
//         $limit: 3
//       },
//       {
//         $project: {
//           _id: 0,
//           productId: "$_id",
//           totalQuantity: 1
//         }
//       }
//     ]);

//     res.json(topThreeProducts);
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: "Internal Server Error" });
//   }
// };