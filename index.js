const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cartRoutes = require("./routes/cartRoutes");

const app = express();

mongoose.connect("mongodb+srv://admin:admin123@zuitt.yipwdbt.mongodb.net/capstone2-ecommerce?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)
app.use("/carts", cartRoutes)

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});