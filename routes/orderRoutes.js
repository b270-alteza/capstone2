const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

router.get("/buy", auth.verify, orderController.buyProducts)

//router.post("/addOrder", auth.verify, orderController.addOrder)

//router.get("/:orderId", orderController.getOrder)

router.get("/getOrderHistory", auth.verify, orderController.getAllMyOrders)

router.get("/getAllOrders", auth.verify, orderController.getAllOrders)

router.get("/getTopThreeProducts", orderController.getTopThreeProducts)

module.exports = router;