const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", userController.checkEmailExists);

router.post("/register", userController.registerUser);

router.post("/login", userController.loginUser);

router.get("/details", auth.verify, userController.getProfile);

router.patch("/changeAdminFlag", auth.verify, userController.changeUserAdminFlag)

router.patch("/changePassword", auth.verify, userController.changeUserPassword)

module.exports = router;