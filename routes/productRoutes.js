const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

router.post("/addProduct", auth.verify, productController.addProduct);

router.get("/allProducts", productController.getAllProducts);

router.get("/", productController.getAllActive);

router.get("/:productId", productController.getProduct);

router.put("/edit/:productId", auth.verify, productController.updateProduct);

router.patch("/archive/:productId", auth.verify, productController.archiveProduct);

//router.get("/getTopThreeProducts", productController.getTopThreeProducts)

module.exports = router;	