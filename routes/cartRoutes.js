const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
const auth = require("../auth");

//router.post("/", auth.verify, orderController.buyProducts)

router.post("/addToCart", auth.verify, cartController.addToCart)

router.post("/editProductCount", auth.verify, cartController.editProductCount)

router.post("/editDeleteProduct", auth.verify, cartController.deleteProduct)

router.get("/getAllInMyCart", auth.verify, cartController.getAllInCartReq)

// router.post("/clearCart", auth.verify, cartController.clearCartReq)

//router.get("/:orderId", orderController.getOrder)

module.exports = router;