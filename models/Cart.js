const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	userId: {
		type: String
	},
	email: {
		type: String
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			productSize: {
				type: String,
				default: "N/A"
			},
			productPrice: {
				type: Number	,
				required: [true, "Price is required"]
			},
			productQuantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			dateAdded: {
				type: Date,
				default: new Date()
			},
			subTotal: {
				type: Number
			}
		}
	]
})
module.exports = mongoose.model("Cart", cartSchema);