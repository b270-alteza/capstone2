const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	cartId : {
		type : String,
		required : [true, "Cart ID is required"]
	},
	orders : [
		{
			orderId: {
				type: String,
				required: [true, "Order Id is required"]
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);	