const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Products name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: [
		{
			type: Number,
			required: [true, "Price is required"]
		}
	],
	size: [
		{
			type: Number
		}
	],
	stock: {
		type: Number,
		required: [true, "Stock is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "Order Id is required"]
			}
		}
	]
})
module.exports = mongoose.model("Product", productSchema);