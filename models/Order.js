const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	orCount: {
		type: Number,
		required: [true, "Order count is required"]
	},
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},
	totalAmount: {
		type: Number,
		required: [true, "Total amount is required"]
	},
	dateOrdered: {
		type: Date,
		default: new Date()
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			productSize: {
				type: String,
				default: "N/A"
			},
			productPrice: {
				type: String,
				required: [true, "Price is required"]
			},
			productQuantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			isPaid: {
				type: Boolean,
				default: true
			}
		}
	]
})
module.exports = mongoose.model("Order", orderSchema);